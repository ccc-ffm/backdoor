#!/usr/bin/python3

import os
import sys
import RPi.GPIO as GPIO
from time import sleep

try:
    sys.argv[1]
except:
    print('Usage: {} open|close'.format(sys.argv[0]))
    sys.exit(1)

pin_open = 20
pin_close = 21
timeout = 4

if sys.argv[1] == 'open':
    pin = pin_open
elif sys.argv[1] == 'close':
    pin = pin_close
else:
    print('unknown command {}'.format(sys.argv[1]))
    sys.exit(2)

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(pin, GPIO.OUT)

GPIO.output(pin, GPIO.LOW)
GPIO.output(pin, GPIO.HIGH) 
sleep(timeout)
GPIO.output(pin, GPIO.LOW)

