#!/usr/bin/python3

import RPi.GPIO as GPIO
import os
import json
import time
import threading
from pidfile import PIDFile

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

base_dir = '/sys/devices/w1_bus_master1/w1_master_slaves'
delete_dir = '/sys/devices/w1_bus_master1/w1_master_remove'
error_count = '/tmp/backdoor_error_count'
last_button = '/tmp/backdoor_last_button'
buttonfile = '/opt/backdoor/data/buttons.json'
pidfile = '/var/run/ibutton.pid'

def checkID(bid):
    with open(buttonfile, 'r') as bf:
        buttons = json.load(bf)
    
    if bid in buttons:
        #open
        os.system('/opt/backdoor/doorlock.py open > /dev/null')
        if not os.path.isfile(error_count):
            open(error_count, 'a').close()
        with open(error_count, 'r+') as efh:
            efh.seek(0)
            efh.write('0')
            efh.truncate()

    else:
        try:
            with open(error_count, 'a+') as efh:
                efh.seek(0,0)
                errors = efh.read()
            if not errors or errors=='':
                errors = 1
            errors = int(errors) + 1
        except BaseException as e:
            errors = 0
        with open(error_count, 'r+') as efhw:
            efhw.seek(0)
            efhw.write(str(errors))
            efhw.truncate()
        if int(errors) >= 9:
            time.sleep(9)
#    time.sleep(1)

def readButton():
    with open(base_dir, 'r') as bfh:
        bid = bfh.read()
        bid = bid.strip()
        
    if 'not found' in bid:
        return False

    checkID(bid)

    with open(delete_dir, 'w') as dfh:
        dfh.write(bid)
        
    with open(last_button, 'w') as lbfh:
        lbfh.write(bid)

if __name__ == '__main__':
    with PIDFile(pidfile): 
        try:
            while True:
                readButton()
                time.sleep(0.1)
        except KeyboardInterrupt:
            pass
        finally:
            GPIO.cleanup()

