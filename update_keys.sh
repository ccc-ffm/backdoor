#!/bin/bash

rm -rf /tmp/backdoorkeys

cp -r /home/open/backdoorkeys/ /tmp/

cd /tmp/backdoorkeys
rows=$(ssh-agent bash -c 'ssh-add /home/backdoor/.ssh/deploy > /dev/null 2>&1; git remote update 2>&1 | wc -l')

if [ $rows -gt 0 ]; then
    cd /home/open/backdoorkeys
    ssh-agent bash -c 'ssh-add /home/backdoor/.ssh/deploy; git pull' > /dev/null 2>&1
    sudo chmod 770 /home/open/.ssh/authorized_keys
    sudo chmod 770 /home/open/.ssh/
    cat * | sed -E 's/(ssh-.{3,20} )/\n\1/g' > /home/open/.ssh/authorized_keys
    sudo chmod 600 /home/open/.ssh/authorized_keys
    sudo chmod 700 /home/open/.ssh/
    echo 'Authorized keys updated.'
    sudo lbu_commit
else
    echo 'Nothing to do. All keys are up to date.'
fi

rm -rf /tmp/backdoorkeys
