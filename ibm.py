#!/usr/bin/env python3

import json
import sys
import pytz
import os
from filelock import FileLock
from datetime import datetime


BUTTON_FILE = '/opt/backdoor/data/buttons.json'
BUTTON_FILE = 'buttons.json'
LOCKFILE = '/tmp/.ibutton.lck'


class IBM:

    def __init__(self):
        self.button_list = {}
        self.load_buttons()

    def get_button_users(self):
        return {id: self.button_list[id]['user'] for id in self.button_list}

    def load_buttons(self):
        """ loads currently active ibuttons from json file """
        with open(BUTTON_FILE, 'r') as fh:
            try:
                ibuttons = json.load(fh)
            except ValueError:
                ibuttons = {}
        self.button_list = ibuttons

    def save_buttons(self):
        """ saves dictionary with active ibuttons to json file"""
        try:
            with open(BUTTON_FILE, 'w') as fh:
                json.dump(self.button_list, fh)
        except IOError as e:
            print('an error occurred while writing the file!')
            print(e)
            sys.exit(20)
        else:
            os.system('sudo /sbin/lbu_commit')
            return True

    def add_ibutton_to_list(self, buttonid, user):
        try:
            self.button_list[buttonid] = {
                'user': user,
                'created': datetime.now(pytz.timezone('Europe/Berlin')).isoformat(sep='T', timespec='seconds')
            }
            self.save_buttons()
        except BaseException as e:
            print('Something went wrong saving the file with the active buttons. Please check it carefully by hand.\n')
            print('Error: {}'.format(e))
            return False
        else:
            print('ID ({buttonid}) added for user ({user})\n'.format(buttonid=buttonid, user=user))
            input('Press <enter> to continue...')
            return True

    def add_ibutton(self):
        """ grant access to ibutton """

        self.load_buttons()
        print('add an iButton to the backdoor\n')
        user = input('nickname: ').strip()
        if not user:
            return False

        button = input('button id: ').strip()
        if not button:
            return False

        if button in self.button_list:
            print('\nERROR: The ID ({}) is already assigned to a different user ({})\n'.format(button, self.button_list[button]))
            sys.exit(10)

        userids = self.get_button_users()

        if user in userids.values():
            print('\nuser exists and has the following IDs assigned:')
            for buttonid in userids:
                if userids[buttonid] == user:
                    print(buttonid)

        save = input('\nAdd ID {id} for user {user}? [yes/NO] '.format(id=button, user=user))

        if save not in ['yes', 'YES', 'Yes']:
            print('changes discarded\n')
            return False

        self.add_ibutton_to_list(button, user)

    def delete_ibutton_from_list(self, button):
        """ deletes an iButton from the list of valid IDs"""

        del(self.button_list[button])
        if self.save_buttons():
            return True
        else:
            return False

    def revoke_ibutton(self):
        """ revoke an iButton from the backdoor """

        print('revoke an iButton from the backdoor\n')
        buttonid = input('enter iButton ID: ').strip()

        if buttonid not in self.button_list:
            print('\nERROR: No active iButton with given ID found.\n')
            input('Press <enter> to continue...')
            return False

        print()
        print('button ID  : {}'.format(buttonid))
        print('assigned to: {}'.format(self.button_list[buttonid]['user']))
        print('assigned at: {}'.format(self.button_list[buttonid]['created']))
        print()
        delete = input('Are you sure you want to revoke this iButton? [yes/NO] ').strip()

        if delete not in ['yes', 'Yes', 'YES']:
            return False

        if self.delete_ibutton_from_list(buttonid):
            print('\nButton with ID {} has been revoked!\n'.format(buttonid))
        else:
            print('Something went terribly wrong. Please check carefully by hand!\n')

        input('Press <enter> to continue...')

    def list_users(self):
        """ prints a list of all users """
        
        users = []
        for user in self.get_button_users().values():
            if user not in users:
                users.append(user)

        print('\nusers:\n')
        print('\n'.join(users))
        print()
        input('Press <enter> to continue...')

    def menu(self):
        """ prints the main menu and return the selected choice """

        print('\n    IBM - iButton Manager')
        print('=' * 30)
        print('\nChoose a task:')
        print('a) add an iButton')
        print('l) list users')
        print('r) revoke an iButton')
        print('\nq) quit iButton manager\n')
        return input('Choice: ').strip()


if __name__ == '__main__':

    ibm = IBM()
    try:
        with FileLock(LOCKFILE, timeout=0):
            choice = False
            while choice not in ['q', 'Q']:
                os.system('clear')
                choice = ibm.menu()
                if choice in ['a', 'A']:
                    ibm.add_ibutton()
                elif choice in ['r', 'R']:
                    ibm.revoke_ibutton()
                elif choice in ['l', 'L']:
                    ibm.list_users()
            print('\nso long and thanks for all the fish\n')

    except TimeoutError:
        os.system('clear')
        print('\n    IBM - iButton Manager')
        print('=' * 30)
        print('\nError: {} is locked by another process.\n'.format(BUTTON_FILE))
        sys.exit(2)

