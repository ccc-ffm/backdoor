#!/bin/bash 
trap "" HUP

function alarm {
	ssh pi@remotecontrol.hq /bin/bash /home/pi/cyberalarm.sh > /dev/null 2>&1 &
	echo '           __                __             '
	echo ' ______ __/ /  ___ _______ _/ /__ _______ _ '
	echo '/ __/ // / _ \/ -_) __/ _ `/ / _ `/ __/  \' \'
	echo '\__/\_, /_.__/\__/_/  \_,_/_/\_,_/_/ /_/_/_/'
	echo '   /___/                                    '
	echo
}

echo "Welcome to the Power-Supply-Center"
echo "1 - Buntes Licht"
echo "2 - Switch"
echo "3 - Alarm"
echo "4 - Dashboard & Labello"
echo "5 - Sound"
echo "6 - Scheinwerfer"
echo "7 - Gadgets Säule Lounge"
read -n 1 -p "Auswahl:" -t 60 var_auswahl
echo ""


if [ "${var_auswahl}" -eq "0" ]
then
        #shutdown HQ
	wget -q -O /dev/null --post-data='{"entity_id": "script.hq_shutdown"}' http://hub.cccffm.space/api/services/script/turn_on
fi

if [ "${var_auswahl}" != "" -a "${var_auswahl}" -ne "0" ] ; then
echo "1 - Einschalten"
echo "2 - Ausschalten"
read -n 1 -p "Auswahl:" -t 60 var_einaus
echo ""

case $var_einaus in
1)
        #einschalten
        case $var_auswahl in 
        1)
                #licht
		wget -q -O /dev/null --post-data='{"entity_id": "light.lounge_cleaninglight_middle"}' http://hub.cccffm.space/api/services/light/turn_on
		;;
	6)
		#scheinwerfer
		wget -q -O /dev/null --post-data='{"entity_id": "switch.lounge_spots"}' http://hub.cccffm.space/api/services/switch/turn_on
		;;
        3)
                #alarm
                alarm #&>/dev/null </dev/null & 
                ;;
        2)
                #switches
		wget -q -O /dev/null --post-data='{"entity_id": "switch.hallway_switch"}' http://hub.cccffm.space/api/services/switch/turn_on
                ;;
        5)
                #sound
		wget -q -O /dev/null --post-data='{"entity_id": "switch.lounge_sound"}' http://hub.cccffm.space/api/services/switch/turn_on
                ;;
	4)	
		#labello
		wget -q -O /dev/null --post-data='{"entity_id": "switch.lounge_hqdash"}' http://hub.cccffm.space/api/services/switch/turn_on
		;;
	7)
		#gadgets_pillar
		wget -q -O /dev/null --post-data='{"entity_id": "switch.lounge_pillar"}' http://hub.cccffm.space/api/services/switch/turn_on
		;;
        *)
                echo "Sorry ich versteh nicht...";;
        esac
;;
2)
        #ausschalten
        case $var_auswahl in
        1)
                #licht
		wget -q -O /dev/null --post-data='{"entity_id": "light.lounge_cleaninglight_middle"}' http://hub.cccffm.space/api/services/light/turn_off
                ;;
        2)
		#switches
		wget -q -O /dev/null --post-data='{"entity_id": "switch.hallway_switch"}' http://hub.cccffm.space/api/services/switch/turn_off
                ;;
        5)
                #sound
		wget -q -O /dev/null --post-data='{"entity_id": "switch.lounge_sound"}' http://hub.cccffm.space/api/services/switch/turn_off
                ;;
        6)
		#scheinwerfer
		wget -q -O /dev/null --post-data='{"entity_id": "switch.lounge_spots"}' http://hub.cccffm.space/api/services/switch/turn_off
                ;;
	4)
		#labello
		wget -q -O /dev/null --post-data='{"entity_id": "switch.lounge_hqdash"}' http://hub.cccffm.space/api/services/switch/turn_off
		;;
	7)
		#gadgets_pillar
		wget -q -O /dev/null --post-data='{"entity_id": "switch.lounge_pillar"}' http://hub.cccffm.space/api/services/switch/turn_off
		;;
        *)
                echo "Sorry ich versteh nicht...";;
        esac
;;
*)
        echo "Sorry ich verstehe nicht....";;
esac
fi

#disown -a
