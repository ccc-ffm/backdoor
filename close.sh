#!/bin/bash

nohup /opt/backdoor/doorlock.py close > /dev/null 2>&1 &

echo ''
echo '               `:+ssss+:`               '
echo '            .omMMMMMMMMMMmo.            '
echo '           oMMMMMMMMMMMMMMMMo           '
echo '          hMMMMMMdo++odMMMMMMh          '
echo '         +MMMMMm-      -mMMMMM+         '
echo '         dMMMMM:        :MMMMMd         '
echo '         dMMMMM-        -MMMMMd         '
echo '         dMMMMM-        -MMMMMd         '
echo '     `---mMMMMM/--------/MMMMMm---`     '
echo '   -dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd-   '
echo '   dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd   '
echo '   dMMMMMmddddddddddddddddddddmMMMMMd   '
echo '   dMMMMM-                    -MMMMMd   '
echo '   dMMMMM-                    -MMMMMd   '
echo '   dMMMMM-                    -MMMMMd   '
echo '   dMMMMM-                    -MMMMMd   '
echo '   dMMMMMo++++++++++++++++++++oMMMMMd   '
echo '   dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd   '
echo '   +MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM+   '
echo '    .osssssssssssssssssssssssssssso.    '
echo ''

